Online E-Commerce Website Management System v6.2017
===============================================
Initial Setup
Adding Essential Files
Admin Login Page
Admin Login Class
Admin Logout Option
Adding Product Category
Showing Category List
Edit Category Name
Update Category Name
Delete Category Name
Add Brand Name
Show Brand List
Update Brand Name
Delete Brand Name
Add Product Page
Product Class
Showing Product List
Product List with Category & Brand
Edit Product Name
Update Product Name
Delete Product Name
Classes for Front View
Load Products
Load Single Product
Add to Cart
Cart Page
Avoid Adding Same Product
Update Cart
Delete Product from Cart
Total Amount or Products
Loading Cart Properly
Products from Brand
Products from Category
Customer Registration Page
Customer Registration Class
Customer Login
Customer Access Control
Customer Logout
Customer Profile
Edit Customer Profile
Payment Page
Payment Option
Offline Order & Payment
Payment Success
Product Ordered List
Showing Ordered Page
Control Order from Admin
View Customer Address
Product Shifting
Remove Shifted Product
Product Compare Option
Product Compare Page
Load Compared Product
Compared Visibility
WishList Option
Load WishList Products
Remove WishList Products
User Access Control